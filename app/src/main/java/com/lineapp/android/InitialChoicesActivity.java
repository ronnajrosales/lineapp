package com.lineapp.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.lineapp.android.login.LoginDialog;
import com.lineapp.android.models.Customer;
import com.lineapp.android.registration.RegistrationDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by RONALYN on 6/3/2017.
 */

public class InitialChoicesActivity extends AppCompatActivity {

    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.initial_choices);
        ButterKnife.bind(this);
        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.fb_login_button);
        loginButton.setReadPermissions("public_profile", "email", "user_friends");
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("facebook", "loginResult::" + loginResult.toString() );

                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                try {
                                    // Application code
                                    Log.d("facebook", "loginResult::" + object.toString() );
                                    String email = object.getString("email");
                                    String firstName = object.getString("first_name");
                                    String lastName = object.getString("last_name");
                                    String password = object.getString("id");
                                    Customer customer = new Customer(firstName, lastName, email, password);

                                    checkIfValidCustomer(customer);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email");
                request.setParameters(parameters); 
                request.executeAsync();


            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });


    }

    @OnClick(R.id.login_button)
    public void login() {
        LoginDialog newFragment = LoginDialog.newInstance();
        newFragment.show(getSupportFragmentManager(), "dialog");
//        Intent intent = new Intent(InitialChoicesActivity.this, LoginActivity.class);
//        startActivity(intent);
    }

    @OnClick(R.id.create_email_button)
    public void registerAccountEmail() {
        // Create the fragment and show it as a dialog.
        RegistrationDialog newFragment = RegistrationDialog.newInstance();
        newFragment.show(getSupportFragmentManager(), "dialog");
//        Intent intent = new Intent(InitialChoicesActivity.this, RegistrationActivity.class);
//        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadCustomerDetails(final Customer customer) {
        Log.d("uploadCustomerDetails", "firstName::" + customer.getFirstName() + " lastName::" + customer.getLastName());
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://cheaphostingph.com/lineappweb/saveprofile.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        if (response.contains("customerid")) {
                            try {
                                Log.d("loginResult", "email::" + customer.getEmailAddress());
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("email", customer.getEmailAddress());
                                jsonObject.put("password", customer.getPassword());
                                jsonObject.put("firstname", customer.getFirstName());
                                jsonObject.put("lastname", customer.getLastName());
                                jsonObject.put("customerid", new JSONObject(response).getString("customerid"));

                                Utils.getInstance(InitialChoicesActivity.this).setCustomerDetails(jsonObject.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Utils.getInstance(InitialChoicesActivity.this).setRegistered(true);
                            Intent intent = new Intent(InitialChoicesActivity.this, QueueActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else if (response.contains("already exists")) {
                            Snackbar.make(findViewById(android.R.id.content), "Account already exists", Snackbar.LENGTH_LONG)
                                    .show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("emailaddress", customer.getEmailAddress());
                params.put("firstname", customer.getFirstName());
                params.put("lastname", customer.getLastName());
                params.put("password", customer.getPassword());
                Log.d("getParams", Utils.getInstance(InitialChoicesActivity.this).getRegistrationId());
                params.put("registrationid", Utils.getInstance(InitialChoicesActivity.this).getRegistrationId());


                return params;
            }
        };
        queue.add(postRequest);
    }

    private void checkIfValidCustomer(final Customer customer) {
        Log.d("Response", "checkIfValidCustomer" );
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://cheaphostingph.com/lineappweb/verify.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        if (!response.equals("")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                Utils.getInstance(InitialChoicesActivity.this).setMyQueueNumber(jsonObject.getInt("myqueuenumber"));

                                jsonObject.put("email", customer.getEmailAddress());
                                jsonObject.put("password", customer.getPassword());

                                Utils.getInstance(InitialChoicesActivity.this).setCustomerDetails(jsonObject.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Utils.getInstance(InitialChoicesActivity.this).setLoggged(true);
                            Intent intent = new Intent(InitialChoicesActivity.this, QueueActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        } else {
                            Log.d("Response", "uploadCustomerDetails" );
                            uploadCustomerDetails(customer);
//                            Snackbar.make(findViewById(android.R.id.content), "Invalid email or password.", Snackbar.LENGTH_LONG)
//                                    .show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", customer.getEmailAddress());
                params.put("password", customer.getPassword());


                return params;
            }
        };
        queue.add(postRequest);
    }

}
