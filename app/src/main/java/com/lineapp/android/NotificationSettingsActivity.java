package com.lineapp.android;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Rosales on 8/6/2017.
 */

public class NotificationSettingsActivity extends Activity {

    @BindView(R.id.cb_20_nums)
    CheckBox cb20NumsAway;

    @BindView(R.id.cb_15_nums)
    CheckBox cb15NumsAway;

    @BindView(R.id.cb_10_nums)
    CheckBox cb10NumsAway;

    @BindView(R.id.cb_5_nums)
    CheckBox cb5NumsAway;

    @BindView(R.id.cb_3_nums)
    CheckBox cb3NumsAway;

    @BindView(R.id.et_custom_1)
    EditText etCustom1;

    @BindView(R.id.et_custom_2)
    EditText etCustom2;

    @BindView(R.id.et_custom_3)
    EditText etCustom3;

    @BindView(R.id.et_custom_4)
    EditText etCustom4;

    @BindView(R.id.et_custom_5)
    EditText etCustom5;

    @BindView(R.id.enter_notif)
    TextView enterButton;

    List<Integer> notification = new ArrayList<>();
    Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_settings_popup);
        ButterKnife.bind(this);
        utils = Utils.getInstance(this);
        setNotificationSettings();
    }

    private void setNotificationSettings() {
        if(utils.getNotifCheckbox1())
            cb20NumsAway.setChecked(true);

        if(utils.getNotifCheckbox2())
            cb15NumsAway.setChecked(true);

        if(utils.getNotifCheckbox3())
            cb10NumsAway.setChecked(true);

        if(utils.getNotifCheckbox4())
            cb5NumsAway.setChecked(true);

        if(utils.getNotifCheckbox5())
            cb3NumsAway.setChecked(true);

        if(utils.getNotifCustom1() > 0)
            etCustom1.setText(String.valueOf(utils.getNotifCustom1()));

        if(utils.getNotifCustom2() > 0)
            etCustom2.setText(String.valueOf(utils.getNotifCustom2()));

        if(utils.getNotifCustom3() > 0)
            etCustom3.setText(String.valueOf(utils.getNotifCustom3()));

        if(utils.getNotifCustom4() > 0)
            etCustom4.setText(String.valueOf(utils.getNotifCustom4()));

        if(utils.getNotifCustom5() > 0)
            etCustom5.setText(String.valueOf(utils.getNotifCustom5()));

    }

    private void updateNotificationSettings() {
        Log.d("updateNotificationSettings", "notif::" + String.valueOf(notification.size()));
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://cheaphostingph.com/lineappweb/updatenotificationsettings.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        if (response.contains("200")) {
                            finish();
                        } else {
                            Snackbar.make(findViewById(android.R.id.content), "Something went wrong.", Snackbar.LENGTH_LONG)
                                    .show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                for (int i = 1; i <= notification.size(); i++) {
                    Log.d("params1", "i: " + String.valueOf(i));
                    if (i == 1) {
                        params.put("notification", String.valueOf(notification.get(i - 1)));
                        Log.d("params1", "notification: " + String.valueOf(notification.get(i - 1)));
                    } else {
                        params.put("notification" + i, String.valueOf(notification.get(i - 1)));
                        Log.d("params2", "notification" + i + ": " + String.valueOf(notification.get(i - 1)));
                    }
                }

                int remainingNotif = 10 - notification.size();
                int startNotifCount = notification.size() + 1;
                for (int i = 0; i < remainingNotif; i++) {
                    params.put("notification" + startNotifCount, String.valueOf(0));
                    Log.d("params3", "notification" + startNotifCount + ": " + String.valueOf(0));
                    startNotifCount++;
                }
                try {
                    JSONObject jsonObject = new JSONObject(Utils.getInstance(NotificationSettingsActivity.this).getCustomerDetails());
                    params.put("customerid", jsonObject.getString("customerid"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("notification", "params::" + params);
                return params;
            }
        };
        queue.add(postRequest);
    }

    @OnClick(R.id.enter_notif)
    public void saveNotificationSettings() {
        Log.d("updateNotification", "onClicke");
        notification.clear();
        if (cb20NumsAway.isChecked()) {
            notification.add(20);
            utils.setNotifCheckbox1(true);
        } else
            utils.setNotifCheckbox1(false);

        if (cb15NumsAway.isChecked()) {
            notification.add(15);
            utils.setNotifCheckbox2(true);
        } else
            utils.setNotifCheckbox2(false);

        if (cb10NumsAway.isChecked()) {
            notification.add(10);
            utils.setNotifCheckbox3(true);
        } else
            utils.setNotifCheckbox3(false);

        if (cb5NumsAway.isChecked()) {
            notification.add(5);
            utils.setNotifCheckbox4(true);
        } else
            utils.setNotifCheckbox4(false);


        if (cb3NumsAway.isChecked()) {
            notification.add(3);
            utils.setNotifCheckbox5(true);
        } else
            utils.setNotifCheckbox5(false);

        if (!etCustom1.getText().toString().isEmpty()) {
            notification.add(Integer.valueOf(etCustom1.getText().toString()));
            utils.setNotifCustom1(Integer.valueOf(etCustom1.getText().toString()));
        } else
            utils.setNotifCustom1(0);

        if (!etCustom2.getText().toString().isEmpty()) {
            notification.add(Integer.valueOf(etCustom2.getText().toString()));
            utils.setNotifCustom2(Integer.valueOf(etCustom2.getText().toString()));
        } else
            utils.setNotifCustom2(0);

        if (!etCustom3.getText().toString().isEmpty()) {
            notification.add(Integer.valueOf(etCustom3.getText().toString()));
            utils.setNotifCustom3(Integer.valueOf(etCustom3.getText().toString()));
        } else
            utils.setNotifCustom3(0);

        if (!etCustom4.getText().toString().isEmpty()) {
            notification.add(Integer.valueOf(etCustom4.getText().toString()));
            utils.setNotifCustom4(Integer.valueOf(etCustom4.getText().toString()));
        } else
            utils.setNotifCustom4(0);

        if (!etCustom5.getText().toString().isEmpty()) {
            notification.add(Integer.valueOf(etCustom5.getText().toString()));
            utils.setNotifCustom5(Integer.valueOf(etCustom5.getText().toString()));
        } else
            utils.setNotifCustom5(0);

        updateNotificationSettings();
    }
}
