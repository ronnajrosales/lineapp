package com.lineapp.android;

import android.app.Application;
import android.content.Context;


public class LineApp extends Application {

    private static LineApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static LineApp getInstance() {
        return mInstance;
    }

}
