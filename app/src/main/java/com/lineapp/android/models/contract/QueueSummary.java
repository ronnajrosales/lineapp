package com.lineapp.android.models.contract;


public interface QueueSummary {

    int getCurrentNumber();

    void setCurrentNumber(int currentNumber);

    double getAverageServeTime();

    void setAverageServeTime(double averageServeTime);
}
