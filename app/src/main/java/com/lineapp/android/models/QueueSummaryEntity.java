package com.lineapp.android.models;

import com.lineapp.android.models.contract.QueueSummary;

public class QueueSummaryEntity implements QueueSummary {


    @Override
    public int getCurrentNumber() {
        return 0;
    }

    @Override
    public void setCurrentNumber(int currentNumber) {

    }

    @Override
    public double getAverageServeTime() {
        return 0;
    }

    @Override
    public void setAverageServeTime(double averageServeTime) {

    }
}
