package com.lineapp.android.models;

import com.lineapp.android.registration.RegistrationContract;

/**
 * Created by Rosales on 5/6/2017.
 */

public class Customer implements RegistrationContract.QueryParam {
    int notifyCount;
    String firstName;
    String lastName;
    String emailAddress;
    String mobileNumber;
    String password;

    public Customer(String firstName, String lastName, String emailAddress, String mobileNumber, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.mobileNumber = mobileNumber;
        this.password = password;
    }

    public Customer(String firstName, String lastName, String emailAddress, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.password = password;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public String getMobileNumber() {
        return mobileNumber;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public int getNotifyCount() {
        return notifyCount;
    }
}
