package com.lineapp.android.registration;

import com.lineapp.android.models.Customer;

/**
 * Created by Rosales on 5/6/2017.
 */

public interface RegistrationContract {

    interface QueryParam {
        String getFirstName();
        String getLastName();
        String getEmailAddress();
        String getMobileNumber();
        String getPassword();
        int getNotifyCount();

    }

    interface ActionListener{
        boolean isCustomerInfoValid(Customer customer);
    }
}
