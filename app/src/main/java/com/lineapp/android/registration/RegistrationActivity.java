package com.lineapp.android.registration;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.util.Log;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lineapp.android.InitialActivity;
import com.lineapp.android.MainActivity;
import com.lineapp.android.QueueActivity;
import com.lineapp.android.R;
import com.lineapp.android.Utils;
import com.lineapp.android.login.LoginActivity;
import com.lineapp.android.models.Customer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RegistrationActivity extends AppCompatActivity {

    @BindView(R.id.firstname)
    EditText mFirstname;
    @BindView(R.id.lastname)
    EditText mLastname;
    @BindView(R.id.mobilenumber)
    EditText mMobilenumber;
    @BindView(R.id.email)
    EditText mEmail;
    @BindView(R.id.password)
    EditText mPassword;
    @BindView(R.id.register_button)
    Button mRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.register_button)
    public void registerCustomer() {
        Customer customer = new Customer(mFirstname.getText().toString(), mLastname.getText().toString(), mEmail.getText().toString(),
                mMobilenumber.getText().toString(), mPassword.getText().toString());
        uploadCustomerDetails(customer);
    }

    public void startMainActivity() {
        Utils.getInstance(this).setRegistered(true);
        Intent intent = new Intent(RegistrationActivity.this, QueueActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void uploadCustomerDetails(final Customer customer) {
        Log.d("uploadCustomerDetails", "firstName::" + customer.getFirstName() + " lastName::" + customer.getLastName());
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://cheaphostingph.com/lineappweb/saveprofile.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        if (response.contains("customerid")) {
                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("email", customer.getEmailAddress());
                                jsonObject.put("password", customer.getPassword());
                                jsonObject.put("firstname", customer.getFirstName());
                                jsonObject.put("lastname", customer.getLastName());
                                jsonObject.put("cpnumber", customer.getMobileNumber());
                                jsonObject.put("customerid", new JSONObject(response).getString("customerid"));

                                Utils.getInstance(RegistrationActivity.this).setCustomerDetails(jsonObject.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            startMainActivity();
                        } else if (response.contains("already exists")) {
                            Snackbar.make(findViewById(android.R.id.content), "Account already exists", Snackbar.LENGTH_LONG)
                                    .show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("emailaddress", customer.getEmailAddress());
                params.put("firstname", customer.getFirstName());
                params.put("lastname", customer.getLastName());
                params.put("cpnumber", customer.getMobileNumber());
                params.put("password", customer.getPassword());
                Log.d("getParams", Utils.getInstance(RegistrationActivity.this).getRegistrationId());
                params.put("registrationid", Utils.getInstance(RegistrationActivity.this).getRegistrationId());


                return params;
            }
        };
        queue.add(postRequest);
    }
}
