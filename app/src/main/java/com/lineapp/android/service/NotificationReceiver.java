package com.lineapp.android.service;

import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.lineapp.android.R;

/**
 * Created by Rosales on 7/29/2017.
 */

public class NotificationReceiver  extends WakefulBroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            playNotificationSound(context);
            lightDevice(context);
        }

        public void playNotificationSound(Context context) {
            try {
                Uri notification = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.alert);
                Ringtone r = RingtoneManager.getRingtone(context, notification);
                r.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void lightDevice(Context context) {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            boolean isScreenOn = pm.isScreenOn();
            if(isScreenOn==false)
            {
                PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
                wl.acquire(10000);
                PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");

                wl_cpu.acquire(10000);
            }
        }


}
