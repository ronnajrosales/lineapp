package com.lineapp.android.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.lineapp.android.QueueActivity;
import com.lineapp.android.R;

public class MessageService extends FirebaseMessagingService {
    @Override      //this method is called when we received the message from Fcm
    public void onMessageReceived(RemoteMessage remoteMessage) {  //used for notifying the user in the form of notification
        Log.d("firebasenotif", "MessageService onMessageReceived");
        Log.d("firebasenotif", "From: " + remoteMessage.getFrom());
//        Log.d("firebasenotif", "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Log.d("firebasenotif", "Notification Message data: " + remoteMessage.getData().toString());


        if (remoteMessage.getNotification() != null) {
            Intent intent = new Intent(this, QueueActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//            Uri path = Uri.parse("android.resource://com.lineapp.android/" + R.raw.alert);
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
            // set notification properties
            notificationBuilder.setContentTitle(remoteMessage.getNotification().getTitle()); //title of notification
            notificationBuilder.setContentText(remoteMessage.getNotification().getBody()); //in this method we pass extracted message to get actual message we pass parameter remotemessage which contain actual data.
            // tgetData returns the datatype of object that saves information in key value
            //to get value from our message we call method get with the key being message
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher); // change notification icon
            notificationBuilder.setContentIntent(pi);
            notificationBuilder.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.alert));
            notificationBuilder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(0, notificationBuilder.build());




        } else {
            Intent intent = new Intent("GPSLocationUpdates");
            // You can also include some extra data.
            Log.d("firebasenotif", remoteMessage.getData().get("nowserving").toString());
            Log.d("firebasenotif", remoteMessage.getData().get("averagetime").toString());
            Log.d("firebasenotif", remoteMessage.getData().get("isserved").toString());
                intent.putExtra("nowserving", remoteMessage.getData().get("nowserving").toString());
                intent.putExtra("averagetime", remoteMessage.getData().get("averagetime").toString());
                intent.putExtra("isserved", remoteMessage.getData().get("isserved").toString());
                sendBroadcast(intent);

        }



    }

    public void playNotificationSound(Context context) {
        try {
            Uri notification = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.alert);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}