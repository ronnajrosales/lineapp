package com.lineapp.android.service;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.lineapp.android.LineApp;
import com.lineapp.android.QueueActivity;
import com.lineapp.android.R;
import com.lineapp.android.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String REGISTERED_TOKEN = "registeredToken";

    @Override
    public void onTokenRefresh() {
        String recent_token = FirebaseInstanceId.getInstance().getToken();

// get token from the server
        Log.d("firebasenotif", recent_token); // show device token in Log cat View
/* If you want to send messages to this application instance or

succeed this application contributions on the server side, send the

Instance ID token to your app server.

*/

        sendRegistrationToServer(recent_token);

    }

// this method is used to send token to your app server.

    private void sendRegistrationToServer(final String token) {
        Utils.getInstance(LineApp.getInstance()).setRegistrationId(token);
        if (Utils.getInstance(LineApp.getInstance()).isLogged()) {
            updateRegistrationId(token);
        }
    }

    private void updateRegistrationId(final String token) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://cheaphostingph.com/lineappweb/updateprofile.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", "updateRegistrationId::" + response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                try {
                    JSONObject jsonObject = new JSONObject(Utils.getInstance(LineApp.getInstance()).getCustomerDetails());
                    params.put("customerid", jsonObject.getString("customerid"));
                    params.put("registrationid", token);
                    Utils.getInstance(LineApp.getInstance()).setRegistrationId(token);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return params;
            }
        };
        queue.add(postRequest);
    }

}
