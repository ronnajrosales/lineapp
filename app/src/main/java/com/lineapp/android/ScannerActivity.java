package com.lineapp.android;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler, ReceiveResultInterface {
    private ZXingScannerView mScannerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startQrScanner();
        setContentView(mScannerView);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mScannerView != null)
            mScannerView.stopCamera();   // Stop camera on pause<br />
    }

    @Override
    public void handleResult(Result result) {

        Log.d("qrScanner", "result::" + result.getText());
        Log.d("qrScanner", "barcode::" + result.getBarcodeFormat().toString());

        DialogFragment newFragment = MyAlertDialogFragment.newInstance(result.getText(), this);
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    public void startQrScanner() {
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.<br />
        mScannerView.startCamera();         // Start camera<br />
    }

    @Override
    public void handleQueueNumber(String queue) {
        Intent intent = new Intent();
        intent.putExtra("queueNumber", queue);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }


    public static class MyAlertDialogFragment extends DialogFragment {

        private static ReceiveResultInterface receiveResultInterface;

        public static MyAlertDialogFragment newInstance(String title, ReceiveResultInterface receiveResultInterface) {
            MyAlertDialogFragment frag = new MyAlertDialogFragment();
            Bundle args = new Bundle();
            args.putString("queueNumber", title);
            MyAlertDialogFragment.receiveResultInterface = receiveResultInterface;
            frag.setArguments(args);
            return frag;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final String queueNumber = getArguments().getString("queueNumber");

            return new AlertDialog.Builder(getActivity())
                    .setMessage("You Queue Number is " + queueNumber)
                    .setPositiveButton(R.string.alert_dialog_ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    receiveResultInterface.handleQueueNumber(queueNumber);
                                }
                            }
                    )
                    .setNegativeButton(R.string.alert_dialog_cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
//                                    mScannerView.startCamera();
                                }
                            }
                    )
                    .create();
        }
    }
}
