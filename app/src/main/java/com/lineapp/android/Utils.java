package com.lineapp.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Utils {

    private SharedPreferences mPrefs;
    private static Utils instance;
    private Context mContext;
    private static final String PREF_NAME = "LINE_APP_PREF";
    private static final String IS_REGISTERED = "isRegistered";
    private static final String IS_LOGGED = "isLogged";
    private static final String REGISTRATION_ID = "registration_id";
    private static final String SERVING_NUMBER = "servingNumber";
    private static final String AVERAGE_TIME = "averageTime";
    private static final String MY_QUEUE_NUMBER = "queueNumber";
    private static final String CUSTOMER_DETAILS = "customerDetails";

    private static final String NOTIF_CB_1 = "NOTIF_CB_1";
    private static final String NOTIF_CB_2 = "NOTIF_CB_2";
    private static final String NOTIF_CB_3 = "NOTIF_CB_3";
    private static final String NOTIF_CB_4 = "NOTIF_CB_4";
    private static final String NOTIF_CB_5 = "NOTIF_CB_5";

    private static final String NOTIF_ET_1 = "NOTIF_ET_1";
    private static final String NOTIF_ET_2 = "NOTIF_ET_2";
    private static final String NOTIF_ET_3 = "NOTIF_ET_3";
    private static final String NOTIF_ET_4 = "NOTIF_ET_4";
    private static final String NOTIF_ET_5 = "NOTIF_ET_5";

    private Utils(Context context) {
        mContext = context;
        mPrefs = context.getSharedPreferences(PREF_NAME, 0);
    }

    public static Utils getInstance(Context context) {

        if (instance == null)
            instance = new Utils(context);

        return instance;
    }

    public void setLoggged (boolean isLogged) {
        mPrefs.edit().putBoolean(IS_LOGGED, isLogged).commit();
    }

    public boolean isLogged () {
        return mPrefs.getBoolean(IS_LOGGED, false);
    }

    public void setRegistered (boolean isRegistered) {
        mPrefs.edit().putBoolean(IS_REGISTERED, isRegistered).commit();
    }

    public boolean isRegistered () {
        return mPrefs.getBoolean(IS_REGISTERED, false);
    }

    public int getServingNumber() {
        return mPrefs.getInt(SERVING_NUMBER, 0);
    }

    public void setServingNumber(int currentNumber) {
        mPrefs.edit().putInt(SERVING_NUMBER, currentNumber).commit();
    }

    public String getAverageServeTime() {
        return mPrefs.getString(AVERAGE_TIME, "");
    }

    public void setAverageServeTime(String averageServeTime) {
        mPrefs.edit().putString(AVERAGE_TIME, averageServeTime).commit();
    }

    public int getMyQueueNumber() {
        return mPrefs.getInt(MY_QUEUE_NUMBER, -1);
    }

    public void setMyQueueNumber(int queueNumber) {
        mPrefs.edit().putInt(MY_QUEUE_NUMBER, queueNumber).commit();
    }

    public void setRegistrationId (String email) {
        mPrefs.edit().putString(REGISTRATION_ID, email).commit();
    }

    public String getRegistrationId () {
        return mPrefs.getString(REGISTRATION_ID, "");
    }
    public void setCustomerDetails (String customerDetails) {
        mPrefs.edit().putString(CUSTOMER_DETAILS, customerDetails).commit();
    }

    public String getCustomerDetails () {
        return mPrefs.getString(CUSTOMER_DETAILS, "");
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    // NOTIFICATION SETTINGS
    public void setNotifCheckbox1 (boolean isSelected) {
        mPrefs.edit().putBoolean(NOTIF_CB_1, isSelected).commit();
    }

    public boolean getNotifCheckbox1 () {
        return mPrefs.getBoolean(NOTIF_CB_1, false);
    }

    public void setNotifCheckbox2 (boolean isSelected) {
        mPrefs.edit().putBoolean(NOTIF_CB_2, isSelected).commit();
    }

    public boolean getNotifCheckbox2 () {
        return mPrefs.getBoolean(NOTIF_CB_2, false);
    }

    public void setNotifCheckbox3 (boolean isSelected) {
        mPrefs.edit().putBoolean(NOTIF_CB_3, isSelected).commit();
    }

    public boolean getNotifCheckbox3 () {
        return mPrefs.getBoolean(NOTIF_CB_3, false);
    }

    public void setNotifCheckbox4 (boolean isSelected) {
        mPrefs.edit().putBoolean(NOTIF_CB_4, isSelected).commit();
    }

    public boolean getNotifCheckbox4 () {
        return mPrefs.getBoolean(NOTIF_CB_4, false);
    }

    public void setNotifCheckbox5 (boolean isSelected) {
        mPrefs.edit().putBoolean(NOTIF_CB_5, isSelected).commit();
    }

    public boolean getNotifCheckbox5 () {
        return mPrefs.getBoolean(NOTIF_CB_5, false);
    }



    public void setNotifCustom1 (int custom) {
        mPrefs.edit().putInt(NOTIF_ET_1, custom).commit();
    }

    public int getNotifCustom1 () {
        return mPrefs.getInt(NOTIF_ET_1, 0);
    }

    public void setNotifCustom2 (int custom) {
        mPrefs.edit().putInt(NOTIF_ET_2, custom).commit();
    }

    public int getNotifCustom2 () {
        return mPrefs.getInt(NOTIF_ET_2, 0);
    }

    public void setNotifCustom3 (int custom) {
        mPrefs.edit().putInt(NOTIF_ET_3, custom).commit();
    }

    public int getNotifCustom3 () {
        return mPrefs.getInt(NOTIF_ET_3, 0);
    }

    public void setNotifCustom4 (int custom) {
        mPrefs.edit().putInt(NOTIF_ET_4, custom).commit();
    }

    public int getNotifCustom4 () {
        return mPrefs.getInt(NOTIF_ET_4, 0);
    }

    public void setNotifCustom5 (int custom) {
        mPrefs.edit().putInt(NOTIF_ET_5, custom).commit();
    }

    public int getNotifCustom5 () {
        return mPrefs.getInt(NOTIF_ET_5, 0);
    }

}
