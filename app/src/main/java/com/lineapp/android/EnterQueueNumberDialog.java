package com.lineapp.android;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;

/**
 * Created by RONALYN on 6/4/2017.
 */

public class EnterQueueNumberDialog extends DialogFragment {

    static EnterQueueNumberDialog newInstance(String title) {
        EnterQueueNumberDialog fragment = new EnterQueueNumberDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final EditText inputText = new EditText(getActivity());

        return new AlertDialog.Builder(getActivity())
                .setTitle("Enter Digital Code:")
                .setView(inputText)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                ((QueueActivity) getActivity()).sendQueueNumber(inputText.getText().toString());
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                dismiss();
                            }
                        }).create();
    }
}

