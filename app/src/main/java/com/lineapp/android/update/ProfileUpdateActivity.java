package com.lineapp.android.update;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lineapp.android.QueueActivity;
import com.lineapp.android.R;
import com.lineapp.android.Utils;
import com.lineapp.android.models.Customer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProfileUpdateActivity extends AppCompatActivity {

    @BindView(R.id.firstname)
    EditText mFirstname;
    @BindView(R.id.lastname)
    EditText mLastname;
    @BindView(R.id.mobilenumber)
    EditText mMobilenumber;
    @BindView(R.id.email)
    EditText mEmail;
    @BindView(R.id.password)
    EditText mPassword;
    @BindView(R.id.register_button)
    Button mRegister;
    @BindView(R.id.register_link)
    TextView registerLink;
    @BindView(R.id.header)
    TextView header;
    @BindView(R.id.notification)
    RadioGroup rgNotification;
    @BindView(R.id.thirty_counts)
    RadioButton thirtyCounts;
    @BindView(R.id.twenty_counts)
    RadioButton twentyCounts;
    @BindView(R.id.fifteen_counts)
    RadioButton fifteenCounts;
    @BindView(R.id.ten_counts)
    RadioButton tenCounts;

    private String customerId;
    private String password;
    private int count;
    private String email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        setCustomerDetails();

    }

    private void setCustomerDetails() {

        header.setText("PROFILE");
        mRegister.setText("Update Profile");
        registerLink.setVisibility(View.GONE);
        mPassword.setVisibility(View.GONE);
        rgNotification.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (thirtyCounts.isChecked())
                    count = 30;
                else if(twentyCounts.isChecked())
                    count = 20;
                else if(fifteenCounts.isChecked())
                    count = 15;
                else if(tenCounts.isChecked())
                    count = 10;

            }
        });

        try {
            JSONObject jsonObject = new JSONObject(Utils.getInstance(this).getCustomerDetails());

            mFirstname.setText(jsonObject.getString("firstname"));
            mLastname.setText(jsonObject.getString("lastname"));
            mEmail.setText(jsonObject.getString("email"));
            mMobilenumber.setText(jsonObject.has("cpnumber") ? jsonObject.getString("cpnumber") : "");

            mEmail.setEnabled(false);
            customerId = jsonObject.getString("customerid");
            password = jsonObject.getString("password");
            email = jsonObject.getString("email");

            String notification = jsonObject.has("notification") ? jsonObject.getString("notification") : "";
            if (notification.equals("30")) {
                thirtyCounts.setChecked(true);
            } else if (notification.equals("20")) {
                twentyCounts.setChecked(true);
            } else if (notification.equals("15")) {
                fifteenCounts.setChecked(true);
            } else if (notification.equals("10")) {
                tenCounts.setChecked(true);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.register_button)
    public void registerCustomer() {
        Customer customer = new Customer(mFirstname.getText().toString(), mLastname.getText().toString(), mEmail.getText().toString(),
                mMobilenumber.getText().toString(), password);
        updateCustomerDetails(customer);
    }

    public void startMainActivity() {
        Utils.getInstance(this).setRegistered(true);
        Intent intent = new Intent(ProfileUpdateActivity.this, QueueActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void updateCustomerDetails(final Customer customer) {
        Log.d("uploadCustomerDetails", "firstName::" + customer.getFirstName() + " lastName::" + customer.getLastName());
        Log.d("Response", "count::" + count);
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://cheaphostingph.com/lineappweb/updateprofile.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        if (response.contains("200")) {
                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("email", email);
                                jsonObject.put("password", password);
                                jsonObject.put("firstname", customer.getFirstName());
                                jsonObject.put("lastname", customer.getLastName());
                                jsonObject.put("cpnumber", customer.getMobileNumber());
                                jsonObject.put("customerid", customerId);
                                jsonObject.put("notification", count);

                                Utils.getInstance(ProfileUpdateActivity.this).setCustomerDetails(jsonObject.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Toast.makeText(ProfileUpdateActivity.this, "Account successfully updated.", Toast.LENGTH_LONG)
                                    .show();
                            finish();
                        } else if (response.contains("already exists")) {
                            Snackbar.make(findViewById(android.R.id.content), "Account already exists", Snackbar.LENGTH_LONG)
                                    .show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customerid", customerId);
                params.put("emailaddress", email);
                params.put("firstname", customer.getFirstName());
                params.put("lastname", customer.getLastName());
                params.put("cpnumber", customer.getMobileNumber());
                params.put("password", password);
                params.put("notif", String.valueOf(count));

                return params;
            }
        };
        queue.add(postRequest);
    }
}
