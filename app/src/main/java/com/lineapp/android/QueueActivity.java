package com.lineapp.android;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.lineapp.android.update.ProfileUpdateActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QueueActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final int SCANNER_CODE = 1;

    @BindView(R.id.current_number_label)
    TextView currentNumber;

    @BindView(R.id.average_time_label)
    TextView averageTime;

    @BindView(R.id.your_number)
    TextView yourNumber;

    @BindView(R.id.scan_button)
    Button scanButton;

    @BindView(R.id.enter_code)
    Button enterCode;

    @BindView(R.id.difference)
    TextView difference;

    @BindView(R.id.no_queue)
    LinearLayout noQueue;

    @BindView(R.id.with_queue)
    LinearLayout withQueue;

    @BindView(R.id.notif_button)
    Button notifSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_queue);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        initQueueSummary();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        try {
            JSONObject jsonObject = new JSONObject(Utils.getInstance(this).getCustomerDetails());
            View hView = navigationView.getHeaderView(0);
            TextView nav_user = (TextView) hView.findViewById(R.id.nav_name);
            TextView nav_email = (TextView) hView.findViewById(R.id.nav_email);
            nav_user.setText(jsonObject.getString("firstname") + " " + jsonObject.getString("lastname"));
            nav_email.setText(jsonObject.getString("email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("GPSLocationUpdates"));

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mMessageReceiver, new IntentFilter("GPSLocationUpdates"));


    }

    @Override
    protected void onStart() {
        super.onStart();
        currentNumber.post(new Runnable() {
            @Override
            public void run() {
                initQueueSummary();
            }
        });
    }

    private void initQueueSummary() {
        try {
            saveCurrentNumberAndAverageTime();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        updateQueueDisplay();

    }

    private void updateQueueDisplay() {
        Utils utils = Utils.getInstance(this);
        yourNumber.setText(String.valueOf(utils.getMyQueueNumber()));
        currentNumber.setText(String.valueOf(utils.getServingNumber()));
        difference.setText(String.valueOf(utils.getMyQueueNumber() - utils.getServingNumber()));
        averageTime.setText(String.valueOf(utils.getAverageServeTime()));

        if (Utils.getInstance(this).getMyQueueNumber() > 0) {
            yourNumber.setVisibility(View.VISIBLE);
            showQueueViews();
        } else {
            yourNumber.setVisibility(View.GONE);
            hideQueueViews();
        }
    }

    private void displayYourNumber(int queuenumber) {
        yourNumber.setText(String.valueOf(queuenumber));
        difference.setText(String.valueOf(queuenumber - Utils.getInstance(this).getServingNumber()));
        yourNumber.setVisibility(View.VISIBLE);
        showQueueViews();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.queue, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            Intent intent = new Intent(QueueActivity.this, ProfileUpdateActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_logout) {
            executeLogout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void executeLogout() {
        if (LoginManager.getInstance() != null)
            LoginManager.getInstance().logOut();
        Utils.getInstance(this).setLoggged(false);
        Utils.getInstance(this).setCustomerDetails("");
        Utils.getInstance(this).setMyQueueNumber(-1);
        Intent intent = new Intent(QueueActivity.this, InitialChoicesActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.scan_button)
    public void initiateQrScanner() {
        Intent intent = new Intent(this, ScannerActivity.class);
        startActivityForResult(intent, SCANNER_CODE);
    }

    @OnClick(R.id.enter_code)
    public void enterQueueNumber() {
        DialogFragment newFragment = EnterQueueNumberDialog.newInstance("Enter your queue number:");
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mMessageReceiver);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SCANNER_CODE) {

            if (resultCode == Activity.RESULT_OK) {
                String queue = data.getStringExtra("queueNumber");
                sendQueueNumber(Integer.valueOf(queue));
                // do something with the result

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // some stuff that will happen if there's no result
            }
        }
    }

    public void sendQueueNumber(final int queueNumber) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://cheaphostingph.com/lineappweb/setqueuenumber.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", "sendQueueNumber::" + response);
                        if (response.contains("200")) {
                            Utils.getInstance(LineApp.getInstance()).setMyQueueNumber(queueNumber);
                            displayYourNumber(queueNumber);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                try {
                    JSONObject jsonObject = new JSONObject(Utils.getInstance(LineApp.getInstance()).getCustomerDetails());
                    params.put("customerid", jsonObject.getString("customerid"));
                    params.put("queuenumber", String.valueOf(queueNumber));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return params;
            }
        };
        queue.add(postRequest);
    }

    public void sendQueueNumber(final String queueNumber) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://cheaphostingph.com/lineappweb/setqueuenumberdigitalcode.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", "sendQueueNumber::" + response);
                        if (response.contains("200")) {
                            try {
                                JSONObject json = new JSONObject(response);
                                int que = json.getInt("queuenumber");
                                if (que > 0) {
                                    Utils.getInstance(LineApp.getInstance()).setMyQueueNumber(que);
                                    displayYourNumber(que);
                                } else
                                    Snackbar.make(findViewById(android.R.id.content), "Invalid digital code.", Snackbar.LENGTH_LONG)
                                            .show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (response.contains("taken"))
                            Snackbar.make(findViewById(android.R.id.content), "Already taken.", Snackbar.LENGTH_LONG)
                                    .show();
                        else
                            Snackbar.make(findViewById(android.R.id.content), "Invalid digital code.", Snackbar.LENGTH_LONG)
                                    .show();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                try {
                    JSONObject jsonObject = new JSONObject(Utils.getInstance(LineApp.getInstance()).getCustomerDetails());
                    params.put("customerid", jsonObject.getString("customerid"));
                    params.put("digitalcode", String.valueOf(queueNumber));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return params;
            }
        };
        queue.add(postRequest);
    }

    private void saveCurrentNumberAndAverageTime() throws JSONException {
        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject jsonObject = new JSONObject(Utils.getInstance(LineApp.getInstance()).getCustomerDetails());
        final String url = "http://cheaphostingph.com/lineappweb/showcurrentserving.php?customerid=" + jsonObject.getString("customerid")
                + "&myqueuenumber=" + Utils.getInstance(this).getMyQueueNumber();
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", "saveCurrentNumberAndAverageTime::" + response);
                        if (!response.equals("")) {
                            Utils utils = Utils.getInstance(QueueActivity.this);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                utils.setAverageServeTime(jsonObject.getString("averagetime"));
                                utils.setServingNumber(jsonObject.getInt("nowserving"));

                                if (jsonObject.getString("isserved").equals("1")) {
                                    utils.setMyQueueNumber(0);
                                    updateQueueDisplay();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            currentNumber.setText(String.valueOf(utils.getServingNumber()));
                            averageTime.setText(String.valueOf(utils.getAverageServeTime()));

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {


                return null;
            }
        };
        queue.add(postRequest);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String averagetime = intent.getStringExtra("averagetime");
            int nowserving = Integer.valueOf(intent.getStringExtra("nowserving"));
            String isserved = intent.getStringExtra("isserved");
            Log.d("firebasenotif", "nowserving:: " + nowserving);
            Utils utils = Utils.getInstance(QueueActivity.this);
            utils.setAverageServeTime(averagetime);
            utils.setServingNumber(nowserving);

            currentNumber.setText(String.valueOf(utils.getServingNumber()));
            averageTime.setText(String.valueOf(String.valueOf(utils.getAverageServeTime())));
            if (isserved.equals("1")) {
                utils.setMyQueueNumber(0);
            }
            updateQueueDisplay();


            Toast.makeText(context, "update data", Toast.LENGTH_LONG).show();
        }
    };

    private void hideQueueViews() {
        currentNumber.setText("");
        yourNumber.setText("");
        difference.setText("");
        withQueue.setVisibility(View.GONE);
        noQueue.setVisibility(View.VISIBLE);
    }

    private void showQueueViews() {
        currentNumber.setVisibility(View.VISIBLE);
        yourNumber.setVisibility(View.VISIBLE);
        difference.setVisibility(View.VISIBLE);
        withQueue.setVisibility(View.VISIBLE);
        noQueue.setVisibility(View.GONE);
    }

    @OnClick(R.id.notif_button)
    public void onClickNotif() {
        DialogFragment newFragment = NotificationSettingsDialog.newInstance();
        newFragment.show(getSupportFragmentManager(), "dialog");
//        Intent intent = new Intent(QueueActivity.this, NotificationSettingsActivity.class);
//        startActivity(intent);
    }


}
